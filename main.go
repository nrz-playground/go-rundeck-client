package main

import (
	"flag"
	"fmt"
	"time"
	"os"
)


func main() {
	rundeckURL := flag.String("url", "http://localhost", "Rundeck URL")
	rundeckToken := flag.String("token", "secret", "Rundeck API token")
	rundeckJob := flag.String("job", "0000000-0000-0000-0000-000000000000", "Rundeck job ID")
	rundeckArgs := flag.String("apiArgs", "", "Rundeck job arguments " +
		"(see http://rundeck.org/docs/api/index.html#running-a-job)")

	flag.Parse()

	fmt.Printf("Launching rundeck job %s on %s. Arguments: '%s'\n", *rundeckJob, *rundeckURL, *rundeckArgs)

	rClient := NewRundeckClient(rundeckURL, rundeckToken)
	rrr := rClient.LaunchJob(rundeckJob, rundeckArgs)
	fmt.Printf("Job was launched on Rundeck. You can view logs at %s\n", rrr.Permalink)

	fmt.Print("Running")
	for rrr.IsRunning() {
		time.Sleep(time.Second)
		fmt.Print(".")
		rrr = rClient.GetJobExecutionStatus(&rrr.Href)
	}
	fmt.Print("\n")

	if rrr.Status != "succeeded" {
		fmt.Printf("Rundeck job finished with non success status (%s). Please look at the Rundeck log at %s",
			rrr.Status, rrr.Permalink)
		os.Exit(1)
	}

	fmt.Println("Rundeck finished with success !")
}
