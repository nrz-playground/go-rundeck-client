package main

import (
	"net/http"
	"fmt"
	"log"
	"io/ioutil"
)

// RundeckClient the Rundeck Client object
type RundeckClient struct {
	client *http.Client
	url string
	token string
}

// NewRundeckClient Instantiate a new Rundeck Client object
func NewRundeckClient(url *string, token *string) *RundeckClient {
	return &RundeckClient{
		client: &http.Client{},
		url: *url,
		token: *token,
	}
}

func (r *RundeckClient) readRundeckExecBody(resp *http.Response) *RundeckRunResponse {
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalf("Failed to read rundeck response: %s\n", err.Error())
	}

	if resp.StatusCode != 200 {
		log.Fatalf("Rundeck response is not valid. Found: %d. Expected: 200. Body was: %s",
			resp.StatusCode, string(body))
	}

	rrr := &RundeckRunResponse{}
	rrr.read(body)
	return rrr
}

// LaunchJob launches the job and wait for job end
func (r *RundeckClient) LaunchJob(jobID *string, args *string) *RundeckRunResponse {
	rrq := RundeckRunQuery{
		ArgString: *args,
		LogLevel: "INFO",
	}

	b, err := rrq.toBuffer()

	if err != nil {
		log.Fatalln("Unable to create RundeckRunQuery, aborting.")
	}

	req, err := http.NewRequest("POST", fmt.Sprintf("%s/api/18/job/%s/run", r.url, *jobID), b)
	if err != nil {
		log.Fatalf("Unable to create Rundeck HTTP request object")
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Accept", "application/json")
	req.Header.Set("X-Rundeck-Auth-Token", r.token)

	resp, err := r.client.Do(req)

	if err != nil {
		log.Fatalf("Failed to POST run query to Rundeck: %s\n", err.Error())
	}

	defer resp.Body.Close()

	return r.readRundeckExecBody(resp)
}

// GetJobExecutionStatus retrieves the job execution status
// returns a Job RunResponse object
func (r *RundeckClient) GetJobExecutionStatus(url *string) *RundeckRunResponse {
	req, err := http.NewRequest("GET", *url, nil)
	if err != nil {
		log.Fatalf("Unable to create Rundeck HTTP request object")
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Accept", "application/json")
	req.Header.Set("X-Rundeck-Auth-Token", r.token)

	resp, err := r.client.Do(req)

	if err != nil {
		log.Fatalf("Failed to GET job execution status from Rundeck: %s\n", err.Error())
	}

	defer resp.Body.Close()
	return r.readRundeckExecBody(resp)
}
