package main

import (
	"bytes"
	"encoding/json"
)

// RundeckRunQuery the run query
type RundeckRunQuery struct {
	ArgString string `json:"argString"`
	LogLevel string `json:"logLevel"`
}

func (r *RundeckRunQuery) toBuffer() (*bytes.Buffer, error) {
	b := new(bytes.Buffer)
	err := json.NewEncoder(b).Encode(r)
	if err != nil {
		return nil, err
	}
	return b, nil
}

// RundeckRunResponse the rundeck response
// some attributes are stripped
type RundeckRunResponse struct {
	ID int `json:"id"`
	Href string `json:"href"`
	Permalink string `json:"permalink"`
	Status string `json:"status"`
	Project string `json:"project"`
	ExecutionType string `json:"executionType"`
	User string `json:"user"`
	// Other parameters are ignored
}

func (r *RundeckRunResponse) read(b []byte) error {
	return json.Unmarshal(b, r)
}

// IsRunning returns true if the job is running
func (r *RundeckRunResponse) IsRunning() bool {
	return r.Status == "running"
}
