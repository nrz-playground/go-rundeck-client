# Go Rundeck Client

This tool uses the Rundeck API to launch jobs. It was intended to be used in a CI environment like Gitlab CI.

# Gitlab CI step example

```yaml
rundeck:deploy:
  stage: deploy
  image: nerzhul/rundeck-client:1.1
  environment: integration
  only:
    - develop
    - /^release\/.*/
  dependencies:
    - maven:deploy
  script:
    - /usr/bin/go-rundeck-client -url=${RUNDECK_URL} -job="${JOB_ID}" -token=${API_TOKEN} -apiArgs="-myarg1 argvalue1"
```
